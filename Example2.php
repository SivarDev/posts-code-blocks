<?php
//Valores ingresadas por el usuario, puedes cambiar los valores si gustas
$valor1 = 7;
$valor2 = 5;

//Variable que almacenará el resultado de de $valor1 multiplicado por $valor2 
$valor3;

//Proceso 1
//Comparamos "Si el Valor 1 es mayor que el Valor 2"
if ($valor1 > $valor2) {
	echo "{$valor1} es mayor que {$valor2}. ";
} else {
	echo "{$valor2} es mayor que {$valor1}. ";
}

//Proceso 2
//Realizamos la multiplicación y almacenamos el resultado en una variable
$valor3 = $valor1 * $valor2;

//Imprimimos el resultado
echo "El resultado de {$valor1} x {$valor2} es igual a {$valor3}.";
?>
